/**
 * @link      https://sbrdbry@bitbucket.org/sbrdbry/demo.git for the source repository
 * @copyright Copyright (c) 2017 Stuart Bradbury.
 */

import React from 'react';
import ReactDOM from 'react-dom';

import Content from './Content';

/* This part of the code has been removed */

import registerServiceWorker from './registerServiceWorker';
import './index.css';

ReactDOM.render((
     <Router>
       <div>
          <Route exact path="/" render={routeProps => <Content {...routeProps} contentId={0}/>} />
/* This part of the code has been removed */
     ),
     document.getElementById('root')
);

registerServiceWorker();
