/**
 * @link      https://sbrdbry@bitbucket.org/sbrdbry/demo.git for the source repository
 * @copyright Copyright (c) 2017 Stuart Bradbury.
 */

import React, { Component } from 'react';

/* This part of the code has been removed */

class RightSidebar extends Component {		
  render() {
    return (
/* This part of the code has been removed */
					<img src={this.props.angryqem} style={this.props.imgStyle} alt="angryqem" />	
			  </div>			  
			  <div className="sidebar-module">
				<h4>Archives</h4>
				<ol className="list-unstyled">
				  <li><a href="/">March 2017</a></li>
				  <li><a href="/">February 2017</a></li>
				  <li><a href="/">January 2017</a></li>
				  <li><a href="/">December 2016</a></li>
				  <li><a href="/">November 2016</a></li>
				  <li><a href="/">October 2016</a></li>
				  <li><a href="/">September 2016</a></li>
				  <li><a href="/">August 2016</a></li>
				  <li><a href="/">July 2016</a></li>
				  <li><a href="/">June 2016</a></li>
				  <li><a href="/">May 2016</a></li>
				  <li><a href="/">April 2016</a></li>
				</ol>
			  </div>
			  <div className="sidebar-module">
				<h4>Elsewhere</h4>
				<ol className="list-unstyled">
				  <li><a href="/">GitHub</a></li>
				  <li><a href="/">Bitbucket</a></li>
				  <li><a href="/">Codepen</a></li>
				</ol>
			  </div>			  
			</div>	
/* This part of the code has been removed */		
    );
  }
}

export default RightSidebar;