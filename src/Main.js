/**
 * @link      https://sbrdbry@bitbucket.org/sbrdbry/demo.git for the source repository
 * @copyright Copyright (c) 2017 Stuart Bradbury.
 */

/* This part of the code has been removed */
import RightSidebar from './RightSidebar';

class Main extends Component {	
  render() {	  
	const divStyle = {};
	
	const asciiguyStyle = {
		background: 'black',
		paddingLeft: '0',
	};
	
    return (
      <section className="section1"
	  ref={ (sectionElement) => this.sectionElement = sectionElement}
	  >
/* This part of the code has been removed */
				
				
				<RightSidebar imgStyle={this.props.imgStyle} divStyle={divStyle} angryqem={this.props.angryqem} mounted={this.props.mounted} />
				
				<div className="col-sm-2 hidden-xs" style={divStyle}></div>
				
				
			  </div>			  
			</div>
		</div>		
	  </section>
    );
  }
}

export default Main;