/**
 * @link      https://sbrdbry@bitbucket.org/sbrdbry/demo.git for the source repository
 * @copyright Copyright (c) 2017 Stuart Bradbury.
 */

import React, { Component } from 'react';
import {VelocityComponent, VelocityTransitionGroup} from 'velocity-react';

class AppFooter extends Component {	
  render() {
    return (
/* This part of the code has been removed */
				<div className="container">			
				  <div className="row">
					  <div className="col-sm-offset-2 col-sm-6">					  
					  </div>
/* This part of the code has been removed */
		</div>
		</VelocityComponent>		
		</VelocityTransitionGroup>
    );
  }
}

export default AppFooter;