/**
 * @link      https://sbrdbry@bitbucket.org/sbrdbry/demo.git for the source repository
 * @copyright Copyright (c) 2017 Stuart Bradbury.
 */

/* This part of the code has been removed */

class BlogPost1 extends Component {
  render() {
/* This part of the code has been removed */
    return (
/* This part of the code has been removed */
    );
  }
}


class BlogPost2 extends Component {
  render() {
/* This part of the code has been removed */
    return (
/* This part of the code has been removed */

		<p>Cum sociis natoque penatibus et magnis <a href="/">dis parturient montes</a>, nascetur ridiculus mus. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Sed posuere consectetur est at lobortis. Cras mattis consectetur purus sit amet fermentum.</p>
		<blockquote>
		  <p>Curabitur blandit tempus porttitor. <strong>Nullam quis risus eget urna mollis</strong> ornare vel eu leo. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
		</blockquote>
		<p>Etiam porta <em>sem malesuada magna</em> mollis euismod. Cras mattis consectetur purus sit amet fermentum. Aenean lacinia bibendum nulla sed consectetur.</p>
		<p>Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Morbi leo risus, porta ac consectetur ac, vestibulum at eros.</p>
	  </div>
    );
  }
}

class BlogPost3 extends Component {
  render() {
/* This part of the code has been removed */
    return (
	  <div className="blog-post">
		<h2 className="blog-post-title">{title}</h2>
		<p className="blog-post-meta">June 14, 2017 by <a href="/">Chris</a></p>

		<p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aenean lacinia bibendum nulla sed consectetur. Etiam porta sem malesuada magna mollis euismod. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
		<ul>
		  <li>Praesent commodo cursus magna, vel scelerisque nisl consectetur et.</li>
		  <li>Donec id elit non mi porta gravida at eget metus.</li>
/* This part of the code has been removed */
    );
  }
}

class MiddleBit extends Component {	
  render() {
	  
	var content = null;	
	
	var previous = '', next = '', disablePrevious = '', disableNext = '';
		  
/* This part of the code has been removed */
		disablePrevious = 'disabled';
	} else if (this.props.contentId === 1) {
		content = <BlogPost2 title='Products' />;
		previous = "/";
		next = "services";
/* This part of the code has been removed */
	} else if (this.props.contentId === 4) {
		content = <BlogPost3 title='About' />;
		previous = "downloads";
		next = "contact";
/* This part of the code has been removed */
	
    return (
/* This part of the code has been removed */
	
		  {content}
	
	
		  <nav>
			<ul className="pager">
/* This part of the code has been removed */
			</ul>
		  </nav>
	
		</div>		
    );
  }
}

export default MiddleBit;